## CICD Devops Task XCEL

### Architecture Diagram 

  <div align="center">
  <p>
    <img src="https://lh7-us.googleusercontent.com/bXZNqEq6P5Iapj1kPSimK2hB9s2XmjsHnGTWRZIgE-WB3uRFEus5Lel0wTnjWQ9vg043tsAf6JMcpGsDBw8bdp6D3-9fFkjJmaZbsL9tdUIfNpNWL5Z_WFQMk_2nvKWiQfTEbv3WloB1kwUdEHlOigU" alt="Your Image">
  </p>
</div>


1. Basic directory structure
     
        |---Terraform
        |----Dockerfile
        |----app.py
        |----.gitlab-ci.yml
        |----requirements.txt

2. requirements.txt  consists of

   ![](https://lh5.googleusercontent.com/OORtoO5gw-9Ytl6ST2h4qnRC9MGaldONxdtCvH9O5iL11HPc4PXWIJDqZdU8rXpmNXowtEWYpQMNdf5zAUelHySFT-9vqzcP12uD4QnQkRvSK6I-H3GcQV2XGCa_kxERd3yswlwj5Fe8B84LCSk_iZHVYz8mUBjWNRMRoL9oRrhJ3ROnd_5CpTR2J6qvkQ)

3. Make the flask application run locally using python app.py. Tested the root  from the browser and got the expected response

  ![](https://lh7-us.googleusercontent.com/Gq4h4HG6uONROLVWJmisDtlD4lBqmgLF9CEVpQ_WHB5N-Mfd0Ir1TFqn3nmjqsogCmHVY8CmnYs0P--hQUghhH53aBUObyV05eU1YlXB7dk-Vi87D1xeI9fJ78vmDBItb6JwecXibWPbcmBNeGjGd6M)

## Dockerization: Dockerize the Python application
4. Dockerfile is used to create a docker container and consists of 
```
FROM python:3.13-rc-slim

WORKDIR /python-docker

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY . .

EXPOSE 8081

CMD [ "python3","app.py"]
```
Used python 3.8 version and chose rc-slim in order to reduce the size of the docker images. This can help us deploy the docker image faster. Created a working directory as ‘/python-docker’. Copied only requirements.txt because docker images are built layer by layer. Whenever the bottom layer gets changed, all the layers above them are rebuilt. So, application code changes often however, dependencies won't change much. Moreover, building the dependencies takes more time during the building process. Downloading and Installation of all the required dependencies is done using the command pip3 install. The source code is copied to the container and “CMD \[ "python3","app.py"]”. "python3" This is the executable or interpreter for the Python programming language. It tells Docker to use Python 3 to execute the script and "app.py" will be executed when the container starts. It is assumed to be in the current working directory or specified path within the container.

## Provisioning the Ec2 Instance with terraform

In main.tf 
 1. Downloading Aws provider from the terraform module and 
 2. Specified the region we are provisioning 
 3. Provided the configuration of the ec2 Instance with userdata
 4. Creating the Security groups with inboud rules 80 for HTTP, 22 for SSH, 8081 for Python   
 5. Userdata to setup and configure the docker 
    This script updates the system, installs Docker, starts the Docker service, adds the user to the Docker group, and adjusts permissions for Docker access. It's designed for an environment where Docker is being set up on an Amazon EC2 instance.

 ```
 #!/bin/bash -xe

sudo su

sudo yum update
sudo yum install docker -y
sudp systemctl enable docker.service
sudo systemctl start docker.service
sudo systemctl status docker.service
sudo usermod -a -G docker ec2-user
sudo chmod 666 /var/run/docker.sock


 ```
 Initalizing by using 
```
terraform init
```
Plan the changes that will be applied by this configuration
```
terraform plan
```
Apply the planned changes and create resources
```
terraform apply -auto-approve
```

## CI/CD Implementation: Implement a Continuous Integration/Continuous Deployment (CI/CD) pipeline using Gitlab CI/CD.

 CI/CD configuration automates the building and deployment of a Docker image. The image is built, tagged, and pushed to AWS ECR in the 'build' stage. In the 'deploy' stage, it connects to an EC2 instance, pulls the latest image, and runs a Docker container.
```
        image: docker:latest

        variables:
        REPOSITORY_URL: $AWS_ACCOUNT_ID.dkr.ecr.us-east-1.amazonaws.com
        REGION: us-east-1
        DOCKER_HOST: tcp://docker:2375
        DOCKER_DRIVER: overlay2
        DOCKER_TLS_CERTDIR: ""
        IMAGE_TAG: $CI_PIPELINE_IID

        services:
        - docker:dind

        before_script:
        - apk add --no-cache curl jq python3 py3-pip
        - pip install awscli
        - aws ecr get-login-password --region "${REGION}"| docker login --username AWS --password-stdin $REPOSITORY_URL 

        stages:
        - build
        - deploy

        build:
        stage: build
        script:
        - echo "Building image..."
        - docker build -t $REPOSITORY_URL/$ECR_REPO:latest .
        - echo "Tagging image..."
        - docker tag $REPOSITORY_URL/$ECR_REPO:latest $REPOSITORY_URL/$ECR_REPO:$IMAGE_TAG
        - echo "Pushing image..."
        - docker push $REPOSITORY_URL/$ECR_REPO:latest
        - docker push $REPOSITORY_URL/$ECR_REPO:$IMAGE_TAG
        only:
        - main

        deploy:  
        stage: deploy

        before_script:
        - mkdir -p ~/.ssh
        - echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
        - chmod 600 ~/.ssh/id_rsa
        - 'which ssh-agent || ( apt-get update -y && apt-get install openssh-client -y )'
        - eval "$(ssh-agent -s)"
        - ssh-add ~/.ssh/id_rsa

        script:
        - ssh -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no $EC2_USER@$EC2_INSTANCE_IP "docker stop demo-python || true && docker rm demo-python || true"
        - ssh -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no $EC2_USER@$EC2_INSTANCE_IP "aws ecr get-login-password --region "${REGION}"| docker login --username AWS --password-stdin $REPOSITORY_URL"
        - ssh -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no $EC2_USER@$EC2_INSTANCE_IP "docker pull $REPOSITORY_URL/$ECR_REPO:latest"
        - ssh -i ~/.ssh/id_rsa -o StrictHostKeyChecking=no $EC2_USER@$EC2_INSTANCE_IP "docker run -d -p 8081:8081 --name demo-python $REPOSITORY_URL/$ECR_REPO:latest"
        
```
 Make sure to add the variables In Settings -> CICD -> Variables

 ![](https://lh7-us.googleusercontent.com/_v7edxt2eP32RYJNZlLbDVAyS3w4C8XrvXMmgwBXyUOj1YWx4OKW4PGTKna1DnP608Uc3f8naagbsjh6N-Ia_tDqaGShBvWVeuYdSO1LuKh58gspG9NcQ91GiCKpoyXIwPAVpv0Qm0kV0WTdTZgGwkE)

 ## Final Output

 ![](https://lh7-us.googleusercontent.com/Mi_tCAKN6oLd2sUCX66WUCc135_61py3_NzwCQWxjMICqMZjPmQyqFsTqC5La71-J75zXsW0KkBGOB2Q-cji0dGTw0SBP_gsp8IPQ0WQmqI2X_zNv2djJyJ1U1pLmqnsCUWFAEKGPiBPlK9aAQzwrzg)

